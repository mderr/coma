use std::fs;
use std::io::ErrorKind;
use std::path::{Path, PathBuf};

use crate::config::{Config, Contains};
use crate::hash::Hasher;
use crate::path::{self, Converter, Concatenator};
use crate::exception::{Handler, self};
use crate::message;

fn file_has_changed( file_path: &Path, storage_directory_path: &Path ) -> bool {
    let stored_path = storage_directory_path.concatenate(file_path);

    let file_md5 = file_path.get_hash();
    let stored_md5 = stored_path.get_hash();

    if file_md5 == stored_md5 {
        false
    } else {
        true
    }
}

pub fn save_file( from: &Path, to: &Path ) {
    fs::copy(from, to).handle( &from.display().to_string() );
    message::inform( &format!("Stored `{}` in `{}`", from.get_filename(), to.get_string()) );
}

fn track_file( file_path: &Path, storage_directory_paths: &Vec<PathBuf>, config: &mut Config ) {
    for storage_directory_path in storage_directory_paths {
        if !storage_directory_path.exists() {
            let message = format!( "Created `{}` storage directory", storage_directory_path.get_string() );

            fs::create_dir(&storage_directory_path).handle( &storage_directory_path.get_string() );
            message::inform(&message);
        }

        let stored_path = storage_directory_path.concatenate(file_path);

        if stored_path.exists() {
            if file_has_changed(&file_path, storage_directory_path) {
                let override_question = format!( "The file `{}` already exists in the `{}` storage directory, do you wish to override this file", 
                    file_path.get_filename(), 
                    storage_directory_path.get_filename() 
                );

                if message::ask(override_question.as_str(), false) {
                    save_file(file_path, &stored_path);
                }

            } else {
                message::warn( &format!("Skipped copying `{}` into `{}` (No changes)", file_path.get_string(), storage_directory_path.get_string()) );
            }
 
        } else {
            let parent_directories = stored_path.get_parents();

            for parent_directory in parent_directories {
                if !parent_directory.exists() {
                    fs::create_dir(&parent_directory).handle( &parent_directory.display().to_string() );
                }                
            }

            save_file(file_path, &stored_path);
        } 
    }

    add_file_entry(file_path, storage_directory_paths, config);
}


fn add_file_entry( file_path: &Path, storage_directory_paths: &Vec<PathBuf>, config: &mut Config ) {
    let absolute_file_path = file_path.to_absolute();

    if !config.file.contains_file(&absolute_file_path) {
        config.add_tracked_entry(&absolute_file_path, storage_directory_paths)
    }
    
}

fn track_files_in_directory( directory: &Path, storage_directory_paths: &Vec<PathBuf>, config: &mut Config ) {
    let file_paths = path::in_directory(directory).handle(&directory.get_string());

    for file_path in file_paths {
        track_file(&file_path, storage_directory_paths, config);
    }
}

pub fn start_tracking( files: Vec<&String>, storage_directory_paths: &Vec<PathBuf>, config: &mut Config ) {
    for file in files {
        let file_path = Path::new(file);

        if file_path.is_dir() {
            track_files_in_directory(file_path, storage_directory_paths, config);
        } else {
            track_file(file_path, storage_directory_paths, config);
        }
    }
}
