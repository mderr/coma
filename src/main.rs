#![feature(io_error_more)]

mod config;
mod arguments;
mod tracking;
mod exception;
mod input;
mod message;
mod path;
mod hash;
mod restoring;

use config::Config;
use std::env;

use crate::exception::Handler;



pub const NAME: &str        = "Configuration Manager";
pub const VERSION: &str     = "0.1.0";
pub const DESCRIPTION: &str = "a config file manager written in Rust";
pub const COMMAND: &str     = "coma";

const ARROW: &'static str = "❯";


// A homeless man who has lived on the streets of Cleveland, USA for years has shot to fame, after a clip of his mellifuls radio voice went viral. Tedd Williams was filmed by a local cameraman, holding a cardboard sign that read "I am ex-radio announcer who has fallen on hard times".




fn main() {
    //let config: Config = toml::from_str(s)

    let mut config = Config::read();

    let arguments: Vec<String> = env::args().collect();
    
    arguments::read(arguments, &mut config);

    // println!("xxx: {:?}", config.create_missing_storage_directory);
    // println!("xxx: {:?}", config.tracked_files);
    //println!("xxx: {:?}", config.file[0].force_absolute);
    //println!("xxx: {:?}", config.file[1].force_absolute);

    // println!("{}", toml::to_string_pretty(&config).unwrap());

    // let mut serialized_string = String::new();
    // let mut toml_serializer = toml::Serializer::new(&mut serialized_string);

    // config.file.serialize(&mut toml_serializer).unwrap();

    // println!("SANSQHQS {}", serialized_string);


    config.save_to("test.toml").handle("cosa2.toml");
}
