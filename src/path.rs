use std::{path::{Path, PathBuf}, io::Error};
use std::env;

use crate::exception;

pub fn home() -> PathBuf {
    home::home_dir().unwrap()
}

pub fn in_directory( directory: &Path ) -> Result<Vec<PathBuf>, Error> {
    let mut paths_vector: Vec<PathBuf> = Vec::new();

    for entry in directory.read_dir()? {
        if let Ok(entry) = entry {
            let path = entry.path();

            if path.is_dir() {
                paths_vector.append( &mut in_directory(&path)? );

            } else {
                paths_vector.push(path)
            }
        }
    }

    Ok(paths_vector)
}

pub trait Converter {
    fn get_string( &self ) -> String;
    fn get_filename( &self ) -> String;
    fn get_parents( &self ) -> Vec<PathBuf>;
    fn to_absolute( &self ) -> PathBuf;
}

impl Converter for Path {
    fn get_string( &self ) -> String {
        self.display().to_string()
    }

    fn get_filename( &self ) -> String {
        String::from( self.file_name().unwrap().to_str().unwrap() )
    }

    fn get_parents( &self ) -> Vec<PathBuf> {
        let mut parents: Vec<PathBuf> = Vec::new();
        let path_ancestors= self.ancestors().skip(1); // Skip 1 which is self
        
        for path in path_ancestors {
            let path_buf = PathBuf::from( &path.get_string() );
    
            parents.push(path_buf);
        }
    
        parents.pop(); // Remove last which is ""
        parents.reverse();
        
        parents
    }

    fn to_absolute( &self ) -> PathBuf {
        if !self.starts_with("/") {
            env::current_dir().unwrap().concatenate(self)
        } else {
            PathBuf::from( self )
        }
    }
}

pub fn get_validated_directory( storage_path: &Path ) -> PathBuf {
    let mut directory_path_string = storage_path.display().to_string();

    if !directory_path_string.ends_with("/") {
        directory_path_string += "/";
    }

    let directory_path_buf = PathBuf::from(&directory_path_string);

    if !directory_path_buf.exists() {
        exception::raise_error(std::io::ErrorKind::NotFound, &directory_path_string);
    }

    if !directory_path_buf.is_dir() {
        exception::raise_error(std::io::ErrorKind::NotADirectory, &directory_path_string);
    }  
    
    directory_path_buf
}

pub trait Concatenator<T> {
    fn concatenate( &self, other_path: T ) -> PathBuf;
}

impl Concatenator<&Path> for Path {
    fn concatenate( &self, other_path: &Path ) -> PathBuf {
        let validated_directory = get_validated_directory(&self);

        let path_string = format!( "{}{}", 
            validated_directory.display().to_string(), 
            other_path.display().to_string(),
        );

        PathBuf::from(path_string)
    }
}

impl Concatenator<&str> for Path {
    fn concatenate( &self, other_path: &str ) -> PathBuf {
        let validated_directory = get_validated_directory(&self);

        let path_string = format!( "{}{}", 
            validated_directory.display().to_string(), 
            other_path,
        );

        PathBuf::from(path_string)
    }
}