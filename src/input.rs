use std::io::{prelude::*, Error, stdin};
use std::path::Path;
use std::fs::File;

pub fn write_file<T: ?Sized>( file_path: &T, file_contents: &str ) -> Result<(), Error>
    where T: AsRef<Path> {
    let mut file = File::create(file_path)?;

    file.write_all( file_contents.as_bytes() )?;

    Ok(())
}

pub fn read_file<T: ?Sized>( file_path: &T ) -> Result<String, Error>
    where T: AsRef<Path> {
    let mut file = File::open(file_path)?;
    let mut file_contents = String::new();

    file.read_to_string(&mut file_contents)?;

    Ok(file_contents)
}

pub fn read_stdin() -> Result<String, Error> {
    let mut input = String::new();

    stdin().read_line(&mut input)?;

    if let Some('\n') = input.chars().next_back() {
        input.pop();
    }

    Ok(input)
}