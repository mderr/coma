use std::io::{stdout, prelude::*, ErrorKind};
use colored::Colorize;

use crate::{ARROW, exception::{Handler, self}, input, arguments};

const AFFIRMATIVE_CHAR: &'static str = "y";
const NEGATIVE_CHAR: &'static str = "n";

const FORMAT_CHAR: char = '`';

#[derive(PartialEq)]
enum MessageColor {
    Green,
    Red,
    Yellow,
    Blue,
    Purple,
}

fn get_highlighted_text( formated_text: &str, message_color: MessageColor ) -> String {
    let mut text_string = String::from(formated_text);
    let get_format_chars = |string: &String| { string.matches(FORMAT_CHAR).count() };

    if get_format_chars(&text_string) % 2 != 0 {
        exception::raise(exception::CustomErrorKind::Internal, "get highlighted text (format chars not even)");
    }

    while get_format_chars(&text_string) > 0 {
        let words: Vec<&str> = text_string.splitn(3, FORMAT_CHAR).collect();

        let highlighted_word = match message_color {
            MessageColor::Green  => Colorize::green( words[1] ),
            MessageColor::Red    => Colorize::red( words[1] ),
            MessageColor::Yellow => Colorize::yellow( words[1] ),
            MessageColor::Blue   => Colorize::blue( words[1] ),
            MessageColor::Purple => Colorize::purple( words[1] ),
        };

        text_string = format!( "{}{}{}", words[0].white(), highlighted_word, words[2].white() );
    }

    text_string
}

fn print_message( message: &str, message_color: MessageColor, print_newline: bool ) {
    let message = format!("`{}` {}", ARROW, message);
    let formatted_message = get_highlighted_text(&message, message_color);

    print!("{formatted_message}");

    stdout().flush().handle("Flushing stdout"); // Flush to print immedialty

    if print_newline {
        println!();
    }
}

pub fn inform( message: &str ) {
    print_message( message, MessageColor::Green, true );
}

pub fn alert( message: &str ) {
    print_message( message, MessageColor::Red, true );
}

pub fn warn( message: &str ) {
    print_message( message, MessageColor::Yellow, true );
}

pub fn ask( message: &str, default_answer: bool ) -> bool {
    let hint = match default_answer {
        true  => format!( "({}/{})", AFFIRMATIVE_CHAR.to_uppercase().green().bold(), NEGATIVE_CHAR.red() ),
        false => format!( "({}/{})", AFFIRMATIVE_CHAR.green(), NEGATIVE_CHAR.to_uppercase().red().bold() ),
    };

    let message = format!( "{} {}: ", message, hint );

    print_message( message.as_str(), MessageColor::Blue, false );

    let input = input::read_stdin().handle("Reading stdin").to_lowercase();

    let user_response: bool = match input.as_str() {
        "y" | "yes" => true,
        "n" | "no"  => false,
        _ => default_answer,
    };

    user_response
}

pub fn show_help() {
    let name_message = format!( "`{name}` v{version}, {description}",
        name = crate::NAME,
        version = crate::VERSION.bold(),
        description = crate::DESCRIPTION,
    );

    let usage_message = format!( "`Usage:` {command} {options} {folder}\n",
        command = crate::COMMAND.bold(),
        options = "[Options]...".purple(),
        folder = "<File/Folder path>".blue(),
    );

    let options_title = "`Options:`";

    print_message(&name_message, MessageColor::Yellow, true);
    print_message(&usage_message, MessageColor::Red, true);
    print_message(options_title, MessageColor::Purple, true);

    for flag in arguments::FLAGS {
        println!( "  -{short}, --{long}\t\t{description}",
            short = flag.short.to_string().bold(),
            long = flag.long.bold(),
            description = flag.description,
        );
    }
}

pub fn show_version() {
    let version_message = format!("`{name}` version `{version}`", 
        name = crate::NAME,
        version = crate::VERSION
    );

    inform(&version_message);
}
