use std::path::Path;

use md5::{Md5, Digest};

use crate::{exception::Handler, input, path::Converter};

fn bytes_to_hex_string( bytes: Vec<u8> ) -> String {
    let mut hex_representation = String::new();

    for byte in bytes {
        hex_representation.push_str( format!("{:02x}", byte).as_str() )
    }

    hex_representation
}

pub trait Hasher {
    fn get_hash( &self ) -> String;
}

impl Hasher for Path {
    fn get_hash( &self ) -> String {
        let file_contents = input::read_file(self).handle(&self.get_filename());

        let mut hasher = Md5::new();
        hasher.update(file_contents);
        let bytes = hasher.finalize().to_vec();

        bytes_to_hex_string(bytes)
    }
}