use std::{path::{Path, PathBuf}, io::Error};

use toml;

use crate::{input, exception::{Handler, self, CustomErrorKind}, path::{self, Concatenator, Converter}};



use serde::{Deserialize, Serialize, Serializer, ser::SerializeStruct};

const DEFAULT_CONFIG_PATH: &'static str = "/.config/coma/config.toml";

enum SpecialPattern {
    Comment,
    Newline,
    Array,
}

fn get_pretty_array( array_str: &str ) -> String {
    let mut pretty_array = String::new();

    let array_str = &array_str[ ..array_str.len() - 1 ]; // remove ']'
    let array_elements: Vec<&str> = array_str.split(", ").collect();

    pretty_array.push_str(" =\u{200e}[ \n"); // Invisible char '\u{200e}' to avoid infinite recursion when pattern matching

    for element in array_elements {
        pretty_array.push_str( &format!("\t{},\n", element) );
    }

    pretty_array.push_str("]\n");

    pretty_array
}

pub trait FormattedSerializer {
    fn formatted( &self ) -> String;
}

impl FormattedSerializer for String {
    fn formatted( &self ) -> String {
        let mut formatted_string = String::from(self);

        for special_pattern in [ SpecialPattern::Comment, SpecialPattern::Newline, SpecialPattern::Array ] {
            let match_str = match special_pattern {
                SpecialPattern::Comment => "comment",
                SpecialPattern::Newline => "newline",
                SpecialPattern::Array => " = [",
            };
    
            while formatted_string.matches(match_str).count() > 0 {
                let first_vector: Vec<&str> = formatted_string.splitn(2, match_str).collect();
                let second_vector: Vec<&str> = first_vector[1].splitn(2, "\n").collect();
        
                let unmodified_string = first_vector[0];
                let pattern_value = second_vector[0];
                let remaining_string = second_vector[1];
    
                let modified_string = match special_pattern {
                    SpecialPattern::Comment => format!( "# {}\n", &pattern_value[ 4..pattern_value.len() - 1 ] ),
                    SpecialPattern::Newline => String::from( "\n" ),
                    SpecialPattern::Array => get_pretty_array(pattern_value),
                };
    
                formatted_string = [ unmodified_string, &modified_string, remaining_string ].join("");
            }
        }    
    
        formatted_string = formatted_string.replace("\u{200e}", " "); // Replace invisible chars with normal spaces

        formatted_string
    }
}

#[derive(Debug, Clone, Deserialize)]
pub struct TrackedFile {
    pub path: String,
    pub stored_in: Vec<String>,
    pub force_absolute: Option<bool>,
}

#[derive(Debug, Deserialize)]
pub struct Config {
    pub create_missing_storage_directory: bool,
    pub make_all_paths_absolute: bool,
    pub default_storage_path: Option<String>,
    pub always_replace_with_stored: Option<bool>,
    pub tracked_files: Option<Vec<String>>,
    pub nums: Vec<u8>,
    pub file: Option<Vec<TrackedFile>>,
}

pub trait Contains {
    fn contains_file( &self, file_path: &Path ) -> bool;
}

impl Contains for Option<Vec<TrackedFile>> {
    fn contains_file( &self, file_path: &Path ) -> bool {
        match self {
            Some(tracked_files) => {
                for tracked_file in tracked_files {
                    if tracked_file.path == file_path.get_string() {
                        return true
                    } 
                }

                false
            },

            None => false
        }
    }
}


impl Config {
    fn new() -> Config {
        Config {
            create_missing_storage_directory: false,
            make_all_paths_absolute: true,
            default_storage_path: None,
            always_replace_with_stored: None,
            tracked_files: None,
            nums: vec![1,2,3],
            file: None,
        }
    }

    pub fn from( config_path: &Path ) -> Config {
        let file_contents = input::read_file(&config_path ).handle(&config_path.get_string() );
        let config: Config = toml::from_str(&file_contents).unwrap();

        config
    }

    pub fn read() -> Config {
        let config_path = path::home().concatenate(DEFAULT_CONFIG_PATH);

        if config_path.exists() {
            Self::from(&config_path)
        } else {
            Config::new()
        }
    }

    pub fn save_to<T: ?Sized>( &mut self, file_path: &T ) -> Result<(), Error>
        where T: AsRef<Path> {
            let mut serialized_string = String::new();
            let mut toml_serializer = toml::Serializer::new(&mut serialized_string);
        
            self.serialize(&mut toml_serializer).unwrap();
        
            input::write_file( file_path, &serialized_string.formatted() )?;

            Ok(())
    }

    pub fn add_tracked_entry( &mut self, file_path: &Path, storage_directory_paths: &Vec<PathBuf> ) {
        let stored_in: Vec<String> = storage_directory_paths.into_iter().map( |x| x.to_absolute().display().to_string() ).collect();
        
        let entry = TrackedFile {
            path: file_path.display().to_string(),
            stored_in,
            force_absolute: None,
        };

        // match &mut self.file {
        //     Some(mut files) => files.push(entry),
        //     None => self.file = Some( vec![entry] ),
        // }

        if self.file.is_some() {
            self.file.as_mut().unwrap().push(entry)
        } else {
            self.file = Some( vec![entry] );
        }

        
    }
}

pub trait Finder {
    fn get_tracked_file_by_path( &self, file_path_string: &String ) -> &TrackedFile;
}

impl Finder for Vec<TrackedFile> {
    fn get_tracked_file_by_path( &self, file_path_string: &String ) -> &TrackedFile {
        

        for tracked_file in self {
            println!("finding {} in {}", file_path_string, &tracked_file.path);
            if &tracked_file.path == file_path_string {
                return tracked_file;
            }
        }

        exception::raise(CustomErrorKind::Internal, "subject_name666")
    }
}

impl Serialize for TrackedFile {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where S: Serializer {
        let mut state = serializer.serialize_struct("TrackedFile", 2)?;

        state.serialize_field("path", &self.path)?;

        if self.force_absolute.is_some() {
            state.serialize_field("force_absolute", &self.force_absolute)?;
        }

        state.serialize_field("stored_in", &self.stored_in)?;



        state.end()
    }
}

impl Serialize for Config {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
        where S: Serializer {
        let mut state = serializer.serialize_struct("Config", 4)?;

        state.serialize_field("create_missing_storage_directory", &self.create_missing_storage_directory)?;
        state.serialize_field("make_all_paths_absolute", &self.make_all_paths_absolute)?;

        if self.default_storage_path.is_some() {
            state.serialize_field("default_storage_path", &self.default_storage_path)?;
        }

        if self.always_replace_with_stored.is_some() {
            state.serialize_field("always_replace_with_stored", &self.always_replace_with_stored)?;
        }

        state.serialize_field("newline", "")?;
        state.serialize_field("comment", "This is hehe")?;
        state.serialize_field("tracked_files", &self.tracked_files)?;
        state.serialize_field("comment", "value")?;
        state.serialize_field("newline", "")?;
        state.serialize_field("comment", "value2")?;
        state.serialize_field("nums", &self.nums)?;
        state.serialize_field("file", &self.file)?;

        state.end()
    }
}
