//use std::error::Error;
use std::process::exit;
use std::io::{Error, ErrorKind};

use crate::message;

pub struct Exception {
    subject: &'static str,
    action: &'static str,
    suggestion: &'static str,
    exit_code: i32,
}

const GENERIC_ERROR: Exception = Exception {
    subject: "An error", action: "has occured", suggestion: "and it doesn't have a custom message",
    exit_code: 1
};
const OPTION_ERROR: Exception = Exception {
    subject: "Option", action: "was not recognized", suggestion: "try `--help` to see all options",
    exit_code: 64
};
const DIRECTORY_ERROR: Exception = Exception {
    subject: "Directory", action: "is not a directory", suggestion: "please specify a directory path to use as storage",
    exit_code: 65
};
const FILE_ERROR: Exception = Exception {
    subject: "Path", action: "could not be found", suggestion: "please check the spelling",
    exit_code: 66
};
const INTERNAL_ERROR: Exception = Exception {
    subject: "Internal development error when trying to", action: "was raised", suggestion: "please fix",
    exit_code: 70
};
const PERMISSION_ERROR: Exception = Exception {
    subject: "Permission for", action: "was denied", suggestion: "did you mean to run as root?",
    exit_code: 77
};
const NO_STORAGE_DIRS_ERROR: Exception = Exception {
    subject: "No `storage path` could be", action: "and no default path is specified in the config file", suggestion: "please provide a storage path for the files",
    exit_code: 77
};


fn get_exception_message( exception: &Exception, subject_name: &str ) -> String {
    let exception_message =  format!( "{subject} `{subject_name}` {action}, {suggestion}",
        subject = exception.subject,
        subject_name = subject_name,
        action = exception.action,
        suggestion = exception.suggestion,
    );

    exception_message
}

fn handle_error(  error: Error, subject_name: &str ) -> ! {
    let exception: Exception = match error.kind() {
        ErrorKind::NotADirectory => DIRECTORY_ERROR,
        ErrorKind::NotFound => FILE_ERROR,
        ErrorKind::PermissionDenied => PERMISSION_ERROR,
        
        _ => {
            message::alert( get_exception_message(&GENERIC_ERROR, &error.to_string()).as_str() );
            exit( GENERIC_ERROR.exit_code )
        },
    };

    message::alert( get_exception_message(&exception, subject_name).as_str() );
    exit( exception.exit_code )
}

pub enum CustomErrorKind {
    InvalidOption,
    NoStorageDirectories,
    Internal,
}

pub fn raise_error( error_kind: ErrorKind, subject_name: &str ) -> ! {
    let error = Error::from(error_kind);

    handle_error(error, subject_name);
}

pub fn raise( custom_error_kind: CustomErrorKind, subject_name: &str ) -> ! {
    let exception: Exception = match custom_error_kind {
        CustomErrorKind::InvalidOption => OPTION_ERROR,
        CustomErrorKind::NoStorageDirectories => NO_STORAGE_DIRS_ERROR,
        CustomErrorKind::Internal => INTERNAL_ERROR,
        
        _ => GENERIC_ERROR,
    };

    message::alert( get_exception_message(&exception, subject_name).as_str() );

    exit( exception.exit_code )
}

pub trait Handler<T> {
    fn handle( self, subject_name: &str ) -> T;
}

impl<T> Handler<T> for Result<T, Error> {
    fn handle( self, subject_name: &str ) -> T {
        let result: T = match self {
            Ok(ok) => ok,
            Err(err) => handle_error(err, subject_name),
        };

        result
    }
}
