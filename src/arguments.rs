use colored::Colorize;
use std::{io::ErrorKind, path::Path};
use std::path::PathBuf;

use crate::{path, restoring};
use crate::{tracking, exception, message, config::Config};

pub struct Flag {
    pub long: &'static str,
    pub short: char,
    pub description: &'static str
}

pub const HELP_FLAG: Flag    = Flag { long: "help", short: 'h', description: "Show this help message" };
pub const VERSION_FLAG: Flag = Flag { long: "version", short: 'V', description: "Show program version" };
pub const TRACK_FLAG: Flag   = Flag { long: "track", short: 't', description: "Add a file or folder to keep track of" };
pub const DIR_FLAG: Flag     = Flag { long: "dir", short: 'd', description: "Set the path/paths to store files" };
pub const CONFIG_FLAG: Flag  = Flag { long: "config", short: 'C', description: "Set the path/paths to store files" };
pub const RESTORE_FLAG: Flag  = Flag { long: "restore", short: 'R', description: "Set the path/paths to store files" };

pub const FLAGS: [Flag; 6] = [
    HELP_FLAG,
    VERSION_FLAG,
    TRACK_FLAG,
    DIR_FLAG,
    CONFIG_FLAG,
    RESTORE_FLAG,
];

enum ProgramOption {
    Track,
    Dir,
    Config,
    Restore,
    None,
}

pub fn read( arguments: Vec<String>, config: &mut Config ) {
    println!("{:?}", arguments);

    let mut last_option = ProgramOption::None;
    let mut files_to_track: Vec<&String> = Vec::new();

    let mut storage_directory_paths: Vec<PathBuf> = Vec::new();
    let mut paths_to_restore: Vec<String> = Vec::new();

    let mut restoring = false;

    if arguments.len() > 1 {
        for argument in &arguments[1..] {
            println!("{}", argument.green());

            if argument.starts_with("--") {
                match &argument[2..] {
                    x if x == HELP_FLAG.long => message::show_help(),
                    x if x == VERSION_FLAG.long => message::show_version(),
                    x if x == TRACK_FLAG.long => last_option = ProgramOption::Track,
                    x if x == DIR_FLAG.long => last_option = ProgramOption::Dir,
                    x if x == CONFIG_FLAG.long => last_option = ProgramOption::Config,
                    x if x == RESTORE_FLAG.long => {
                        restoring = true;
                        last_option = ProgramOption::Restore;
                    },

                    _ => exception::raise( exception::CustomErrorKind::InvalidOption, argument ),
                }

            } else if argument.starts_with("-") {
                for char in argument.chars().skip(1) {
                    match char {
                        x if x == HELP_FLAG.short => message::show_help(),
                        x if x == VERSION_FLAG.short => message::show_version(),
                        x if x == TRACK_FLAG.short => last_option = ProgramOption::Track,
                        x if x == DIR_FLAG.short => last_option = ProgramOption::Dir,
                        x if x == CONFIG_FLAG.short => last_option = ProgramOption::Config,
                        x if x == RESTORE_FLAG.short => {
                            restoring = true;
                            last_option = ProgramOption::Restore;
                        },

                        _ => exception::raise( exception::CustomErrorKind::InvalidOption, argument ),
                    }
                }
                
            } else {
                match last_option {
                    ProgramOption::Track => files_to_track.push(argument),
                    ProgramOption::Dir => storage_directory_paths.push( PathBuf::from(argument) ),
                    ProgramOption::Config => {
                        *config = Config::from(Path::new(argument));
                        last_option = ProgramOption::None; // We don't want multiple config files
                    },
                    ProgramOption::Restore => paths_to_restore.push( argument.to_string() ),

                    _ => files_to_track.push(argument),
                }
            }
        }
    }

    if  storage_directory_paths.is_empty() {
        if config.default_storage_path.is_some() {
            storage_directory_paths.push( PathBuf::from(config.default_storage_path.as_ref().unwrap()) )
        } else {
            exception::raise(exception::CustomErrorKind::NoStorageDirectories, "found")
        }
    }
    
    if restoring {
        restoring::restore( Some(paths_to_restore), config );
    } 

    tracking::start_tracking(files_to_track, &storage_directory_paths, config);
}