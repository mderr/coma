use std::{path::Path, fs};

use crate::{path::Converter, message, config::{Config, Finder}, exception::{CustomErrorKind, self, Handler}};

fn restore_file( stored_path: &Path, target_path: &Path ) {
    fs::copy(stored_path, target_path).handle( "&stored_path.get_filename()" );

    message::inform( &format!("`Restored` file `{}` from `{}`", 
        target_path.get_filename(), 
        stored_path.parent().unwrap().get_filename())
    );
}

pub fn restore( paths_to_restore: Option<Vec<String>>, config: &mut Config ) {
    match paths_to_restore {
        Some(path_strings) => {
            for path_string in path_strings {
                let target_path = Path::new(&path_string).to_absolute();
                
                

                let stored_path = match &config.file {
                    Some(files) => {
                        let a = &files.get_tracked_file_by_path(&target_path.get_string()).stored_in[0];
                        Path::new( a +  )
                    },
                    None => exception::raise(CustomErrorKind::Internal, "subject_name0")
                };

                println!("target_path {} - {}", target_path.get_string(), stored_path.get_string());

                if target_path.exists() {
                    let message = format!("`{}` already exists in local storage, do you wish to `replace` with stored version?",
                    target_path.get_filename() );

                    if config.always_replace_with_stored.is_some() && config.always_replace_with_stored.unwrap() {
                        restore_file( stored_path, target_path.as_path() );

                    } else {
                        if message::ask(&message, false) {
                            restore_file( stored_path, target_path.as_path() );
                        } else {
                            message::inform( &format!("`Skipped` the restoration of `{}`", target_path.get_filename()) );
                        }
                    }

                } else {
                    restore_file( stored_path, target_path.as_path() );
                }
            }
        },
        None => {},
    }
}